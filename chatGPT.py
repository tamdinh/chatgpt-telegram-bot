from dataclasses import dataclass
from functools import lru_cache
import openai
import time

@dataclass
class Response:
    text: str
    time_cons: int


class ChatGPT:
    def __init__(self, token: str):
        self.openai = openai
        self.openai.api_key = token

    @lru_cache(maxsize=100)
    def create_text(self, prompt: str, model: str = "text-davinci-003", temperature: float = 0.5) -> Response:
        start = time.time()
        try:
            response = self.openai.Completion.create(
                engine=model,
                prompt=prompt,
                temperature=temperature,
                max_tokens=600,
                top_p=1.0,
                frequency_penalty=0.0,
                presence_penalty=0.0
            )
            to_return = response['choices'][0]['text'].strip()
        except openai.error.RateLimitError as e:
            to_return = "Vượt quá giới hạn truy cập. Vui lòng quay lại sau"
            print(e.error)

        end = time.time()

        return Response(to_return, int(round(end - start)))

    # create an code suggestion from codex
    @lru_cache(maxsize=100)
    def create_codex(self, prompt: str, model: str = "code-davinci-002", temperature: float = 0.5) -> Response:
        start = time.time()
        try:
            response = self.openai.Completion.create(
                engine=model,
                prompt=prompt,
                temperature=temperature,
                max_tokens=182,
                top_p=1.0,
                frequency_penalty=0.0,
                presence_penalty=0.0
            )
            to_return = response['choices'][0]['text'].strip()
        except openai.error.RateLimitError as e:
            to_return = "Vượt quá giới hạn truy cập. Vui lòng quay lại sau"
            print(e.error)
        end = time.time()
        return Response(to_return, int(round(end - start)))

    @lru_cache(maxsize=100)
    def create_image(self, prompt: str, model: str = "image-alpha-001", temperature: float = 0.5) -> Response:
        start = time.time()
        try:
            response = self.openai.Image.create(
                prompt=prompt,
                n=1,
                size="512x512"
            )
            image_url = response['data'][0]['url']
        except:
            image_url = None
        end = time.time()
        # return response.content
        return Response(image_url, int(round(end - start)))
