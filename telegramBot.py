import asyncio

import requests
from telegram.constants import ChatAction

from chatGPT import ChatGPT
from telegram import Update, Message, constants
from telegram.error import RetryAfter, BadRequest
from telegram.ext import ApplicationBuilder, ContextTypes, CommandHandler, MessageHandler, filters


class TelegramBot:
    def __init__(self, token: str, ai: ChatGPT, allowed: [int]):
        self.bot_token = token
        self.ai = ai
        self.allowed = allowed

    async def help(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        """
        Handles the /help
        """
        await update.message.reply_text("Tài khoản của bạn: " + str(update.message.from_user.id))
        await update.message.reply_text("/start - Start the bot"
                                        "\n/code - Hỏi các vấn đề về lập trình"
                                        "\n/image - Tạo hình ảnh tùy ý"
                                        "\nGõ chuỗi bất kỳ để hỏi các vấn đề chung chung."
                                        "\nMade with OpenAI ChatGPT")

    def is_allowed(self, user: int) -> bool:
        """
        Check if user in allow list
        """
        return user in self.allowed

    async def start(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
        Handles the /start
        """
        if not self.is_allowed(update.message.from_user.id):
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text="Bạn chưa được phép dùng tôi. Hãy liên hệ người quản lý!")
            return

        await context.bot.send_message(chat_id=update.effective_chat.id,
                                       text="Xin chào! Tôi là Chat bot dùng dịch vụ ChatGPT của OpenAI")

    async def send_typing(self, update: Update, context: ContextTypes.DEFAULT_TYPE, every_seconds: int):
        """
        Sends the typing action
        """

        if not self.is_allowed(update.message.from_user.id):
            # await context.bot.send_message(chat_id=update.effective_chat.id, text="You are not allowed to use me!")
            return

        while True:
            await context.bot.send_chat_action(chat_id=update.effective_chat.id, action=ChatAction.TYPING)
            await asyncio.sleep(every_seconds)

    async def code(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
        Generates text
        """
        if not self.is_allowed(update.message.from_user.id):
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text="Bạn chưa được phép hỏi tôi. Hãy liên hệ người quản lý!")
            return

        typing_task = context.application.create_task(
            self.send_typing(update, context, every_seconds=4)
        )

        message_text = update.message.text[6:]
        if message_text == "":
            await context.bot.send_message(chat_id=update.effective_chat.id, text="/code {prompt}")
        else:
            send = await context.bot.send_message(chat_id=update.effective_chat.id,
                                                  text="Nhận yêu cầu, đang suy nghĩ...")

            text = self.ai.create_codex(message_text)

            await context.bot.editMessageText(chat_id=update.effective_chat.id,
                                              message_id=send.message_id,
                                              text=f"Xử lý mất {text.time_cons} giây")

            await context.bot.send_message(
                chat_id=update.effective_chat.id,
                reply_to_message_id=update.message.message_id,
                text=text.text
            )

        typing_task.cancel()

    async def prompt(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        if update.message.text.startswith("/start") or update.message.text.startswith(
                "/code") or update.message.text.startswith("/image"):
            return
        if update.message.text.startswith("/help"):
            return
        if not self.is_allowed(update.message.from_user.id):
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text="Bạn chưa được phép hỏi tôi. Hãy liên hệ người quản lý!")
            return

        typing_task = context.application.create_task(
            self.send_typing(update, context, every_seconds=4)
        )
        message_text = update.message.text
        if message_text == "":
            await context.bot.send_message(chat_id=update.effective_chat.id, text="Bạn muốn hỏi gì?")
        else:
            send = await context.bot.send_message(chat_id=update.effective_chat.id,
                                                  text="Nhận yêu cầu, đang suy nghĩ...")

            text = self.ai.create_text(message_text)

            await context.bot.editMessageText(chat_id=update.effective_chat.id,
                                              message_id=send.message_id,
                                              text=f"Suy nghĩ mất {text.time_cons} giây.")

            await context.bot.send_message(
                chat_id=update.effective_chat.id,
                reply_to_message_id=update.message.message_id,
                text=text.text
            )
            # await context.bot.send_message(
            #     chat_id=update.effective_chat.id,
            #     text=text.text
            # )

        typing_task.cancel()

    # Generate image from text
    async def image(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
        Generate image from text prompt using codex api from openai
        :param update:
        :param context:
        :return:
        """
        if not self.is_allowed(update.message.from_user.id):
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text="Bạn chưa được phép hỏi tôi. Hãy liên hệ người quản lý!")
            return

        typing_task = context.application.create_task(
            self.send_typing(update, context, every_seconds=4)
        )
        message_text = update.message.text[7:]
        if message_text == "":
            await context.bot.send_message(chat_id=update.effective_chat.id, text="/image {prompt}")
        else:
            send = await context.bot.send_message(chat_id=update.effective_chat.id,
                                                  text="Nhận yêu cầu, đang vẽ...")
            image_res = self.ai.create_image(message_text)
            await context.bot.editMessageText(chat_id=update.effective_chat.id,
                                              message_id=send.message_id,
                                              text=f"Xử lý mất {image_res.time_cons} giây")
            if image_res.text:
                # photo = requests.get(image_res.text)
                await context.bot.send_photo(
                    chat_id=update.effective_chat.id,
                    reply_to_message_id=update.message.message_id,
                    photo=image_res.text
                )

        typing_task.cancel()

    def run(self):
        """
        Run the bot
        """
        application = ApplicationBuilder().token(self.bot_token).build()

        application.add_handler(CommandHandler('start', self.start))
        application.add_handler(CommandHandler('image', self.image))
        application.add_handler(CommandHandler('code', self.code))
        application.add_handler(CommandHandler('help', self.help))
        application.add_handler(MessageHandler(filters.TEXT & filters.USER, self.prompt))

        application.run_polling()
